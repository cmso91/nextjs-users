import React from 'react';
import { Title } from '../components/Title';
import Head from 'next/head';

const index = () => {
  return (
    <>
      <Head>
        <title>Home - First app Vercel NextJS</title>
        <meta name="description" content="En este curso aprendi de nextJS" />
      </Head>
      <div>
          <Title>Home Page</Title>
          <p>Aprendamos NextJS</p>
          <style>
          {`
            p {
              color: darkgray;
            }
            p:hover {
              color: darkred;
            }
          `}
        </style>
      </div>
    </>
    );
};

export default index;
