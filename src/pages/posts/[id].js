import { useEffect, useState } from 'react';
import { Title } from '../../components/Title';
import Link from 'next/link';
import Head from 'next/head';

const post = ({ post }) => {

    const [relatedPosts, setRelatedPosts] = useState();

    useEffect(() => {
      const fetchPostsRelated = async () => {
        const response = await fetch('https://jsonplaceholder.typicode.com/posts');
        const data = await response.json();
        setRelatedPosts(data.slice(1, 5));
      }

      fetchPostsRelated();
    }, [])


    return (
      <>
        <Head>
          <title>Posts: {post.title}  - First app Vercel NextJS</title>
          <meta name="description" content={post.body} />
        </Head>
        <div>
            <Title>Post Details</Title>
            <div className='card'>
                <h2>{post.title}</h2>
                <p>{post.body}</p>
            </div>
            <div className='grid'>
              {relatedPosts?.map(post => (
                <Link key={post.id} href={`/posts/${post.id}`}>
                  <a className='card'>
                    <h3>{post.title}</h3>
                    <p>{post.body}</p>
                  </a>
                </Link>
              ))}
            </div>

            <style jsx>
          {`
            .card {
              margin: 1rem;
              flex-basis: 45%;
              padding: 1.5rem;
              text-align: left;
              color: black;
              text-decoration: none;
              border: 2px solid #eaeaea;
              border-radius: 10px;
              transition: color 0.15s ease, border-color 0.15s ease;
            }
            .card:hover,
            .card:focus,
            .card:active {
              color: #0070f3;
              border-color: #0070f3;
            }
            .card h3 {
              margin: 0 0 1rem 0;
              font-size: 1.5rem;
            }
            .card p {
              margin: 0;
              font-size: 1.25rem;
              line-height: 1.5;
            }
            .grid {
              display: flex;
              flex-wrap: wrap;
              max-width: 800px;
              margin-top: 2rem;
            }
          `}
          </style>
        </div>
    </>
    );
};

export default post;

export async function getServerSideProps({ params }){
  const response = await fetch(`https://jsonplaceholder.typicode.com/posts/${params.id}`);
  const post = await response.json();

  return {
    props: {
      post
    }
  }
}
