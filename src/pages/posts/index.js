import Link from 'next/link';
import Head from 'next/head';
import { Title } from '../../components/Title';

const posts = ({ posts }) => {


  return (
    <>
      <Head>
            <title>Posts - First app Vercel NextJS</title>
            <meta name="description" content="Pagina de Post" />
      </Head>
      <div>
          <Title>Users Page</Title>
          <div className='grid'>
            {posts.map(post => (
              <Link key={post.id} href={`/posts/${post.id}`}>
                <a className='card'>
                  <h3>{post.title}</h3>
                  <p>{post.body}</p>
                </a>
              </Link>
            ))}
          </div>
          <style jsx>
          {`
            .grid {
              display: flex;
              flex-wrap: wrap;
              max-width: 800px;
              margin-top: 2rem;
            }
            .card {
              margin: 1rem;
              flex-basis: 45%;
              padding: 1.5rem;
              color: black;
              text-decoration: none;
              border: 2px solid #eaeaea;
              border-radius: 10px;
              transition: color 0.15s ease, border-color 0.15s ease;
            }
            .card:hover,
            .card:focus,
            .card:active {
              color: #0070f3;
              border-color: #0070f3;
            }
            .card h3 {
              margin: 0 0 1rem 0;
              font-size: 1.5rem;
            }
            .card p {
              margin: 0;
              font-size: 1.25rem;
              line-height: 1.5;
            }
          `}
        </style>
      </div>
    </>
    );
};

export default posts;

export async function getServerSideProps() {
  const response = await fetch('https://jsonplaceholder.typicode.com/posts');
  const posts = await response.json();

  return {
    props: {
      posts
    }
  }
}
